package com.example.slava.locationcluster;


import android.util.Log;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Sender {

    OkHttpClient client;


    public Sender(String url, String lat, String lng) throws IOException {
        client = new OkHttpClient();
        post(url, lat, lng);
    }

   public String post(String url, String lat, String lng) throws IOException {
        RequestBody body = new FormBody.Builder()
                .add("lat", lat)
                .add("lng", lng)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        Log.d("Request", body.toString());
        Log.d("Responce", response.body().string());
        return response.body().string();
    }

}