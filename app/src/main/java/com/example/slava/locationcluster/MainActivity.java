package com.example.slava.locationcluster;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    GPSTracker mGPS;
    TextView tvLoc;
    ProgressBar progressBar;
    Button btnSendLoc;
    CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        mGPS = new GPSTracker(this);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setMax(9);
        tvLoc = (TextView) findViewById(R.id.tvLocation);
        btnSendLoc = (Button) findViewById(R.id.btnSendLoc);
        btnSendLoc.setOnClickListener(this);


        countDownTimer = new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvLoc.setText(String.valueOf((millisUntilFinished / 1000)));
                progressBar.incrementProgressBy(1);
                //here you can have your logic to set text to edittext
            }
            public void onFinish() {
                progressBar.setProgress(0);
                tvLoc.setText("done!");
                btnSendLoc.setClickable(true);
            }
        };


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSendLoc:
                countDownTimer.start();
                if (mGPS.canGetLocation) {
                    mGPS.getLocation();
                    try {
                        tvLoc.setText("Latitude" + mGPS.getLatitude() + "\nLongitude" + mGPS.getLongitude());
                        Thread thread = new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                try {
                                    new Sender(
                                        "http://192.168.1.114:8080/locations",
                                        String.valueOf(mGPS.getLatitude()),
                                        String.valueOf(mGPS.getLongitude())
                                    );
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    tvLoc.setText("Please enable gps and network");
                }
                btnSendLoc.setClickable(false);
                break;
        }
    }

}




